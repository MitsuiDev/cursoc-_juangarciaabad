﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace calculator_project
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonOn_Click(object sender, RoutedEventArgs e)
        {
           resetScreen();
        }

        private void buttonOff_Click(object sender, RoutedEventArgs e)
        {
            Screen.Text = "";
        }

        private void buttonZero_Click(object sender, RoutedEventArgs e)
        {
            if (Screen.Text != "0") resetScreen();
        }

        private void resetScreen()
        {
            Screen.Text = "0";
        }

        private void button9_Click(object sender, RoutedEventArgs e)
        {
            printValue("9");
        }
        private void button8_Click(object sender, RoutedEventArgs e)
        {
            printValue("8");
        }
        private void button7_Click(object sender, RoutedEventArgs e)
        {
            printValue("7");
        }
        private void button6_Click(object sender, RoutedEventArgs e)
        {
            printValue("6");
        }
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            printValue("5");
        }
        private void button4_Click(object sender, RoutedEventArgs e)
        {
            printValue("4");
        }
        private void button3_Click(object sender, RoutedEventArgs e)
        {
            printValue("3");
        }
        private void button2_Click(object sender, RoutedEventArgs e)
        {
            printValue("2");
        }
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            printValue("1");
        }
        private void button0_Click(object sender, RoutedEventArgs e)
        {
            printValue("0");
        }
        private void buttonDecimal_Click(object sender, RoutedEventArgs e)
        {
           if(!Screen.Text.Contains(".")) Screen.Text = Screen.Text + "."; 
        }
        private void buttonSwapPositiveNegative_Click(object sender, RoutedEventArgs e)
        {
            if(!Screen.Text.Contains("-"))
            {
                Screen.Text = "-" + Screen.Text;
            }
            else 
            {
                Screen.Text = Screen.Text.Remove(0,1);
            }
        }

        private void addFirstNumber(string number)
        {
            Screen.Text = number;
        }
        private void addNumber(string number)
        {
            Screen.Text = Screen.Text + number;
        }
        private void printValue(string value)
        {
            if (Screen.Text != "0")
            {
                addNumber(value);
            }
            else
            {
                addFirstNumber(value);
            }
        }

        private void Calculate()
        {
            if(Screen.Text.Contains("+")) 
            {
                string[] valuesString = Screen.Text.Split('+');

                double value1 = Convert.ToDouble(valuesString[0]);
                double value2 = Convert.ToDouble(valuesString[1]);
                double value3 = value1 + value2;

                Screen.Text = value3.ToString(); 
            }

            if (Screen.Text.Contains("/"))
            {
                string[] valuesString = Screen.Text.Split('/');

                double value1 = Convert.ToDouble(valuesString[0]);
                double value2 = Convert.ToDouble(valuesString[1]);
                double value3 = Math.Round(value1 / value2, 4);
               

                Screen.Text = value3.ToString();
            }

            if (Screen.Text.Contains("-"))
            {
                string[] valuesString = Screen.Text.Split('-');

                double value1 = Convert.ToDouble(valuesString[0]);
                double value2 = Convert.ToDouble(valuesString[1]);
                double value3 = value1 - value2;

                Screen.Text = value3.ToString();
            }

            if (Screen.Text.Contains("x"))
            {
                string[] valuesString = Screen.Text.Split('x');

                double value1 = Convert.ToDouble(valuesString[0]);
                double value2 = Convert.ToDouble(valuesString[1]);
                double value3 = value1 * value2;

                Screen.Text = value3.ToString();
            }
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            addNumber("+");
        }

        private void buttonDivide_Click(object sender, RoutedEventArgs e)
        {
            addNumber("/");
        }

        private void buttonMultiply_Click(object sender, RoutedEventArgs e)
        {
            addNumber("x");
        }

        private void buttonSustract_Click(object sender, RoutedEventArgs e)
        {
            addNumber("-");
        }

        private void buttonEqual_Click(object sender, RoutedEventArgs e)
        {
            Calculate();
        }

    }
}
