﻿using System;
using System.Collections.Generic;

namespace letras_repetidas
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<char> names = new List<char>() 
            {
                'J','U','A','N'
            };
            List<char> surnames = new List<char>()
            {
                'G','A','R','C','I','A'
            };//TODO FASE 4 APARTIR DE AQUI
            Dictionary<char, int> nameDictionary = new Dictionary<char, int>();

            for(int i = 0; i < names.Count; i++)
            {
                if (names[i] == 'A' || names[i] == 'E' || names[i] == 'I' || names[i] == 'O' || names[i] == 'U')
                {
                    Console.WriteLine($"{names[i]} es una VOCAL");
                }
                else if(names[i] == '0' ||
                    names[i] == '1' ||
                    names[i] == '2' || 
                    names[i] == '3' ||
                    names[i] == '4' || 
                    names[i] == '5' || 
                    names[i] == '6' || 
                    names[i] == '7' ||
                    names[i] == '8' ||
                    names[i] == '9')
                {
                    Console.WriteLine("Los nombres de personas no contienen números");
                }
                else
                {
                    Console.WriteLine($"{names[i]} es una CONSONANTE");
                }
            }
            foreach (char letter in names)
            {
                if (!nameDictionary.ContainsKey(letter))
                {
                    nameDictionary.Add(letter, 1);
                }
                else
                {
                    nameDictionary[letter]++;
                }
            }
            foreach (char letter in names)
            {
                Console.WriteLine(letter);
            }
        }
    }
}
