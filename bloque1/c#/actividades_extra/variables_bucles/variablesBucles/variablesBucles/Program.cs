﻿using System;

namespace variablesBucles
{
    internal class Program
    {
        static void Main(string[] args)
        {
            const int anioBisiesto = 1948;
            const int frecuenciaAnioBisiesto = 4;
            string name = "Juan";
            string surname1 = "Garcia";
            string surname2 = "Abad";
            string completeName = $"Mi nombre es {name} {surname1} {surname2}";
            string nacimientoBisiesto = "El año en que nací fué bisiesto";
            string nacimientoNoBisiesto = "El año en que nací no fué bisiesto";
            int dayOfBorn = 3;
            int monthOfBorn = 7;
            int yearOfBorn = 1981;
            string completeBornDate = $"Nací el {dayOfBorn}/{monthOfBorn}/{yearOfBorn}";
            bool isAnioBisiesto = false;

            Console.WriteLine(completeName);
            Console.WriteLine(completeBornDate);

            for(int i=anioBisiesto; i <= yearOfBorn; i+=frecuenciaAnioBisiesto)
            {
                if(i == yearOfBorn) isAnioBisiesto = true;
            }

            if (isAnioBisiesto) Console.WriteLine(nacimientoBisiesto);
            if (!isAnioBisiesto) Console.WriteLine(nacimientoNoBisiesto);
        }
       
    }
}
