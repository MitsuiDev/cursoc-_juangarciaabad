﻿using System.Collections.Generic;

namespace notas_alumnos_ejercicio.Lib.Models
{
    public  class Student : Entity
    {
        public string Name { get; set; }
        public string Dni { get; set; }
        public List <Exam> Exams {  get; set; }

        public Student()
        {
            Exams = new List <Exam>();
        }

        public bool IsExamAdded(Exam exam)
        {
            exam.Student = this;
            Exams.Add(exam);

            return true;
        }
    }
}
