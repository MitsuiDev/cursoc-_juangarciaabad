﻿namespace notas_alumnos_ejercicio.Lib.Models
{
    public class Subject : Entity
    {
        public string Name { get; set; }
        public string Teacher { get; set; }
    }
}
