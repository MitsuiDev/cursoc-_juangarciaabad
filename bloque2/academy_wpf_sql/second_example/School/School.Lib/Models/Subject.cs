﻿using System;
using System.Collections.Generic;
using System.Text;

namespace School.Lib.Models
{
    public class Subject : Entity
    {
        public string Name { get; set; }

        public string Code { get; set; }
        public ICollection<Enrollment> Enrollments { get; set; }
    }
}
